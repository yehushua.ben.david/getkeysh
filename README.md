# getkey.sh
"getkey.sh" is a Bash script that reads input from the terminal and maps it to a corresponding key or key sequence.

Usage  
To use the script, simply run:
```
./getkey.sh [timeout]
```
If no timeout is specified, the script will block until a key is pressed.   
If a timeout is specified, the script will wait for the specified number of seconds for input and then exit if no input is received.

# Key Mappings
The script maps input to the following keys:

F1 through F12 keys  
Shift + F1 through F12 keys  
Ctrl + F1 through F12 keys  
Ctrl + Shift + F1 through F12 keys  
Shift + Alt + F1 through F12 keys  
Ctrl + Home, End, Page Up, and Page Down keys  
Arrow keys: Up, Down, Left, and Right  
Shift + Arrow keys  
Ctrl + Arrow keys  
Ctrl + Shift + Arrow keys  
Shift + Alt + Arrow keys  
Home and End keys  
Shift + Home and End keys  
Delete and Ctrl + Delete keys  
Esc, Backspace, Tab, Enter, and Space keys  
Any other character is mapped to "CHAR_character"  

# License
Do what you want to do with. This script should not cause any trouble but if it appends ... not my fault.

# Credits
This script was written by Yehushua Ben David (yehushua.ben.david@gmail.com).
