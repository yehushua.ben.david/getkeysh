#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ] ; then 
  echo -e "Usage: $(basename $0) [timeout]
Without timeout $(basename $0) is blocking."
  exit 
fi

function checkSeq() {
  case $1 in

  27.91.49.126)      echo "KEY_HOME";;
  27.91.50.126)      echo "KEY_INSERT";;
  27.91.51.126)      echo "KEY_DELETE";;
  27.91.52.126)      echo "KEY_END";;
  27.91.53.126)      echo "KEY_PAGEUP";;
  27.91.54.126)      echo "KEY_PAGEDOWN";;
  
  27.91.49.59.52.80) echo "SHIFT_ALT_F1";;
  27.91.49.59.52.81) echo "SHIFT_ALT_F2";;
  27.91.49.59.52.82) echo "SHIFT_ALT_F3";;
  27.91.49.59.52.83) echo "SHIFT_ALT_F4";;
  27.91.49.53.59.52.126) echo "SHIFT_ALT_F5";;
  27.91.49.55.59.52.126) echo "SHIFT_ALT_F6";;
  27.91.49.56.59.52.126) echo "SHIFT_ALT_F7";;
  27.91.49.57.59.52.126) echo "SHIFT_ALT_F8";;
  27.91.50.48.59.52.126) echo "SHIFT_ALT_F9";;
  27.91.50.49.59.52.126) echo "SHIFT_ALT_F10";;
  27.91.50.51.59.52.126) echo "SHIFT_ALT_F11";;
  27.91.50.52.59.52.126) echo "SHIFT_ALT_F12";;
  27.91.51.59.52.126) echo "SHIFT_ALT_DELETE";;

  27.79.80)	         echo "KEY_F1";;
  27.79.81)	         echo "KEY_F2";;
  27.79.82)	         echo "KEY_F3";;
  27.79.83)	         echo "KEY_F4";;
  27.91.49.53.126)   echo "KEY_F5";;
  27.91.49.55.126)   echo "KEY_F6";;
  27.91.49.56.126)   echo "KEY_F7";;
  27.91.49.57.126)   echo "KEY_F8";;
  27.91.50.48.126)   echo "KEY_F9";;
  27.91.50.49.126)   echo "KEY_F10";;
  27.91.50.51.126)   echo "KEY_F11";;
  27.91.50.52.126)   echo "KEY_F12";;

27.91.49.59.50.80)      echo "SHIFT_F1";;
27.91.49.59.50.81)      echo "SHIFT_F2";;
27.91.49.59.50.82)      echo "SHIFT_F3";;
27.91.49.59.50.83)      echo "SHIFT_F4";;
27.91.49.53.59.50.126)  echo "SHIFT_F5";;
27.91.49.55.59.50.126)  echo "SHIFT_F6";;
27.91.49.56.59.50.126)  echo "SHIFT_F7";;
27.91.49.57.59.50.126)  echo "SHIFT_F8";;
27.91.50.48.59.50.126)  echo "SHIFT_F9";;
27.91.50.49.59.50.126)  echo "SHIFT_F10";;
27.91.50.51.59.50.126)  echo "SHIFT_F11";;
27.91.50.52.59.50.126)  echo "SHIFT_F12";;
27.91.51.59.50.126)     echo "SHIFT_DELETE";;
27.91.51.59.53.126)     echo "CTRL_DELETE";;

27.91.49.59.54.80) echo "CTRL_SHIFT_F1";;
27.91.49.59.54.81) echo "CTRL_SHIFT_F2";;
27.91.49.59.54.82) echo "CTRL_SHIFT_F3";;
27.91.49.59.54.83) echo "CTRL_SHIFT_F4";;
27.91.49.53.59.54.126) echo "CTRL_SHIFT_F5";;
27.91.49.55.59.54.126) echo "CTRL_SHIFT_F6";;
27.91.49.56.59.54.126) echo "CTRL_SHIFT_F7";;
27.91.49.57.59.54.126) echo "CTRL_SHIFT_F8";;
27.91.50.48.59.54.126) echo "CTRL_SHIFT_F9";;
27.91.50.49.59.54.126) echo "CTRL_SHIFT_F10";;
27.91.50.51.59.54.126) echo "CTRL_SHIFT_F11";;
27.91.50.52.59.54.126) echo "CTRL_SHIFT_F12";;
27.91.51.59.54.126) echo "CTRL_SHIFT_DELETE";;


27.91.49.59.50.72) echo "SHIFT_HOME";;
27.91.49.59.50.70) echo "SHIFT_END";;

  27.91.65)          echo "KEY_UP";; 
  27.91.66)          echo "KEY_DOWN";; 
  27.91.67)          echo "KEY_RIGHT";; 
  27.91.68)          echo "KEY_LEFT";;

  27.91.49.59.53.68) echo "CTRL_LEFT";; 
  27.91.49.59.53.67) echo "CTRL_RIGHT";;
  27.91.49.59.53.65) echo "CTRL_UP";;
  27.91.49.59.53.66) echo "CTRL_DOWN";;

  27.91.49.59.50.68) echo "SHIFT_LEFT";; 
  27.91.49.59.50.67) echo "SHIFT_RIGHT";;
  27.91.49.59.50.65) echo "SHIFT_UP";;
  27.91.49.59.50.66) echo "SHIFT_DOWN";;

  27.91.49.59.54.68) echo "CTRL_SHIFT_LEFT";;
  27.91.49.59.54.67) echo "CTRL_SHIFT_RIGHT";;
  27.91.49.59.54.65) echo "CTRL_SHIFT_UP";;
  27.91.49.59.54.66) echo "CTRL_SHIFT_DOWN";;

  27.91.49.59.54.72) echo "CTRL_SHIFT_HOME";;
  27.91.49.59.54.70) echo "CTRL_SHIFT_END";;


  27.91.49.59.52.68) echo "SHIFT_ALT_LEFT";;
  27.91.49.59.52.67) echo "SHIFT_ALT_RIGHT";;
  27.91.49.59.52.65) echo "SHIFT_ALT_UP";;
  27.91.49.59.52.66) echo "SHIFT_ALT_DOWN";;


  27.91.49.59.53.72) echo "CTRL_HOME";;
  27.91.49.59.53.70) echo "CTRL_END";;
  27.91.53.59.53.126) echo "CTRL_PAGEUP";;
  27.91.54.59.53.126) echo "CTRL_PAGEDOWN";;
  27.27)             echo "KEY_ESC";;
  *)                return 1;;
  esac 
}

if [ $#  -gt 0 ] ; then 
  t="-t $1"
fi
if read -rsN 1 $t  K ; then 
 NUMKEY=`printf "%d" "'$K<"`
 if (( NUMKEY == 27 )) ; then
  SEQ="27" 
  while read -rsN 1 -t 0.01 K ; do
   sleep 0.001	  
   NUMKEY=`printf "%d" "'$K<"` 
   SEQ+=.$NUMKEY 
   if checkSeq $SEQ ; then 
     exit
   fi
  done

  # Uncomment the echo command to help you to add new sequence:
  # echo -e "\033[91m$SEQ) echo \"\";;\033[m" 
  if [ "$SEQ" == 27 ] ; then 
    echo "KEY_ESC"
  fi
 else 
  case $NUMKEY in
   127) echo "KEY_BACK";;
   9) echo "KEY_TAB";;	  
   10) echo "KEY_ENTER";;	 
   32) echo "KEY_SPACE";; 
   *)echo "CHAR_$K";;
  esac
 fi
else 
 echo NOKEY
fi
