#!/bin/bash

FT="\
0000\
1110\
0100\
0000"
FI="\
0200\
0200\
0200\
0200"
FL="\
0300\
0300\
0330\
0000"
FJ="\
0040\
0040\
0440\
0000"
FC="\
0000\
0550\
0550\
0000"
FS="\
0000\
0066\
0660\
0000"
FZ="\
0000\
7700\
0770\
0000"
ALL_F=($FT $FI $FL $FJ $FC $FS $FZ)


fat() {
    local x=$1
    local y=$2
    local F=$3
    local v
    local i
    for (( i=0 ; i<4;i++)) ; do
        v=${F:$i:1}
        if [ "$v" != "0" ]; then
            tat $((x+i)) $y "\033[30;4${v}m--\033[m"
        fi
    done
}
tat() {
    echo -en "\033[$2;$1H$3"
}
rotatePaternLeft() {
    local F=$1
    echo "${F:3:1}${F:7:1}${F:11:1}${F:15:1}${F:2:1}${F:6:1}${F:10:1}${F:14:1}${F:1:1}${F:5:1}${F:9:1}${F:13:1}${F:0:1}${F:4:1}${F:8:1}${F:12:1}"
}

printFormAt(){
    local x=$1
    local y=$2
    local frm=$3
    fat $x $y ${frm:0:4}
    fat $x $((y+1)) ${frm:4:4}
    fat $x $((y+2)) ${frm:8:4}
    fat $x $((y+3)) ${frm:12:4}
}

initBoard() {
    BOARD=""
    local i
    for ((i=0;i<(WW*HH);i++)) ; do
        BOARD+="0"
    done
}
getScreenSize(){
    SW=`tput cols`
    SH=`tput lines`
}

printThisBoard() {
    local xos=$((SW/2-WW))
    local yos=$((SH/2-HH/2))
    local brd=$1
    local cln
    local x
    local y
    for ((y=0;y<HH;y++)) ; do
        cln=""
        for ((x=0;x<WW;x++)) ; do
            local bp=$((y*WW+x))
            local prt="| "
            if ! [ "${brd:$bp:1}" == "0" ] ; then
                prt="\033[4${brd:$bp:1}m${brd:$bp:1} \033[m"
            fi
            cln+=$prt
        done
        tat $((xos)) $((yos+y)) "#$cln#\033[K"
    done
    echo -e "\033[J"
    
    
}


AddFormAtBroad(){
    local bb="$1"
    local x="$2"
    local y="$3"
    local fr="$4"
    #tat 1 30 ${bb}
    for ((i=0;i<WW*HH;i++)) ; do
        local bx=$((i%WW))
        local by=$((i/WW))
        if (( bx>=x &&  bx<=(x+3) && by>=y &&  by<=(y+3)  )) ; then
            local ii=$(( (by-y)*4+(bx-x) ))
            if [ "${fr:ii:1}" == "0" ] ; then
                echo -n ${bb:i:1}
            else
                echo -n ${fr:ii:1}
            fi
        else
            echo -n ${bb:i:1}
        fi
    done
}

validateState() {
    local BRD="$1"
    local fr="$2"
    local x="$3"
    local y="$4"
    local fx=0
    local fy=0
    local fri=0
    local bi=0
    for ((fy=0;fy<4;fy++)) ; do
        for ((fx=0;fx<4;fx++)) ; do
            fri=$((fy*4+fx))
            if [ "${fr:fri:1}" == "0" ] ; then continue ;fi
            if (( (y+fy) >= HH )); then
                return 1
            fi
            if (( (x+fx)<0 || (x+fx) >= WW )); then
                return 1
            fi
            bi=$(( (fy+y)*WW + (fx+x)))
            if [ "${BRD:bi:1}" == "0" ] ; then
                continue
            fi
            return 1
        done
    done
}



getTick() {
    date +%s%N
}


getNexTick() {
    local t=`getTick`
    local d=$1
    echo $((t+d))
}


cleanLines() {
    local BRD=$1
    local nb=0
    local nbln=0
    local rst=""
    local i
    local ln=""
    local y
    local x
    for ((y=0;y<HH;y++)) ; do
        ln=""
        nb=0
        for ((x=0;x<WW;x++)) ; do
          i=$((y*WW+x))
          ln+=${BRD:i:1}
          if ! [ "${BRD:i:1}" == 0 ]; then 
            ((nb++))
          fi
        done
        if ((nb==WW)); then
         rst=$LINE$rst
         ((nbln++))
        else  
         rst+=$ln 
        fi      
    done
    echo $rst 
    return $nbln
}

playGame() {
    getScreenSize
    # clear
    local NEXTCF=${ALL_F[$(( RANDOM % ${#ALL_F[*]} )) ]}
    local CF=${ALL_F[$(( RANDOM % ${#ALL_F[*]} )) ]}
    local TTCF=$CF
    local x=$((WW/2-2))
    local y=1
    local brd
    local lastState
    local dlay=1000000000
    local nexTick=`getNexTick $dlay`
    local tick=`getTick`
    local score=0
    
    
    while :; do
        tick=`getTick`
        BOARD=`cleanLines $BOARD`
        score=$[ score + $? ]
        tat $((SW/2-WW/2-5)) $((SH/2-HH/2-1)) "Score $score\033[K"
        tat 0 0 " \033[K"
        if ! [ "$lastState" == "$x$y$CF" ] ; then
            getScreenSize  
            brd=`AddFormAtBroad $BOARD $x $y "$CF"`
            printThisBoard $brd
            printFormAt $((SW/2+WW/2+10)) $((SH/2-HH/2)) $NEXTCF
        fi
        lastState="$x$y$CF"
        
        if (( tick > nexTick )); then
            if  validateState $BOARD "$CF" $x $((y+1)) ; then
                ((y++));
            else
                BOARD=$brd ;
                ((x=WW/2-2)) ;
                y=0;
                CF=$NEXTCF
                NEXTCF=${ALL_F[$(( RANDOM % ${#ALL_F[*]} )) ]}
                if ! validateState $BOARD "$CF" $x $y ; then
                    tat 1 $((SH/2)) "\033[K"
                    tat $((SW/2 - 6  )) $((SH/2)) "GAME OVER LOSER"
                    initBoard
                    score=0
                    sleep 5
                    ../getkey.sh
                fi
            fi
            nexTick=`getNexTick $dlay`
            continue
        fi
        case `../getkey.sh 0.01` in
            CTRL_UP)   ((y--));;
            KEY_UP)
                until
                CF="$(rotatePaternLeft "$CF")";
                validateState $BOARD $CF $x $y ;
                do
                    :;
                done
            ;;
            KEY_LEFT)   validateState $BOARD "$CF" $((x-1)) $y && ((x--));;
            KEY_RIGHT) validateState $BOARD "$CF" $((x+1)) $y && ((x++));;
            KEY_DOWN)  validateState $BOARD "$CF" $x $((y+1)) && ((y++));;
            KEY_SPACE) while
                validateState $BOARD "$CF" $x $((y+1))
                do
                    ((y++))
                    nexTick=0 #`getNexTick $dlay`;
                done
        esac
    done
    
}

startGame() {
    SPEED=1
    WW=10
    LINE="0"
    while [ ${#LINE} -lt $WW ]; do 
      LINE+=$LINE
    done
    LINE=${LINE:0:WW}
    HH=20
    initBoard
    playGame
}


present() {
    local c
    clear
    getScreenSize
    for c in  T E T R I "S " - " By " "Yehushua." "ben." David; do
        TXT+=$c
        o=${#TXT}
        tat $((SW/2-(o/2))) $(((SH/2))) "$TXT"
        sleep 0.2
    done
    tat  $(( SW/2-(o/2) + 3 )) $((1+(SH/2))) "(Press any key to start)"
    ../getkey.sh 
}

function on_exit {
  reset 
}
trap on_exit EXIT
reset
tput civis
present
clear
startGame
